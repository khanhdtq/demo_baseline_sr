import os 
import numpy as np
import pandas as pd 
from csv import writer
import librosa
from pathlib import Path
from typing import Optional, Union
import torch
from scipy.spatial.distance import cdist

from pyannote.audio.applications.speaker_embedding import SpeakerEmbedding
from pyannote.audio.features import Pretrained, Precomputed

from .config import VALIDATE_DIR, DATABASE_PATH, EMBS_PATH, METADATA_PATH
from .config import SAMPLING_RATE, THRESHOLD

def get_embedding(test_wave,
                  validate_dir: Path,
                  duration: Optional[float] = None,
                  step: float = 0.25,
                  device: Optional[torch.device] = None,
                  batch_size: int = 32
                  ):

    test_wave = test_wave[:, np.newaxis]
    pretrained = Pretrained(validate_dir=validate_dir,
                            duration=duration,
                            step=step,
                            batch_size=batch_size,
                            device=device)

    embedding = pretrained({'waveform': test_wave})

    return embedding

def calc_score(emb1, emb2):
    return cdist(np.mean(emb1, axis=0, keepdims=True),
                np.mean(emb2, axis=0, keepdims=True),
                metric='cosine')[0, 0]

def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

def enroll(wavs, speaker_name):
    # wavs is a list of np.array

    # - get speaker embedding by averaging all utterance embeddings belonging to that speaker 
    spkr_emb = np.zeros((1, 512)) 
    for wav in wavs:
        emb = get_embedding(wav, validate_dir=Path(VALIDATE_DIR), batch_size=1, device='cpu')
        spkr_emb = spkr_emb + emb 
    
    spkr_emb = spkr_emb / len(wavs)

    # - enroll speaker embedding into metadata ----------------------------------------------
    try:
        metadata_df = pd.read_csv(METADATA_PATH)
        new_id = str(metadata_df.shape[0] + 1)
        # print('metadata exists.')
        append_list_as_row(METADATA_PATH, [new_id, speaker_name])
        
    except:
        new_id = '1'
        metadata_df = pd.DataFrame([[new_id, speaker_name]], columns=['ID', 'name'])
        metadata_df.to_csv(METADATA_PATH, index=False)

    # - save speaker embedding to database and send confirmation message -------------------
    np.save(os.path.join(EMBS_PATH, f'{new_id}.npy'), spkr_emb)

    msg = f'You are enrolled under name {speaker_name} and ID {new_id}.'
    return msg 

def speaker_recognizer(speech):

    # - get speaker embedding -------------------------------------------------------------
    emb = get_embedding(speech, validate_dir=Path(VALIDATE_DIR), batch_size=1, device='cpu')

    # - get cosine distance of that embedding with all enrolled embeddings ----------------
    if len(os.listdir(EMBS_PATH)) == 0:
        return None 
    
    emb_fnames = []
    emb_scores = []
    for emb_fname in os.listdir(EMBS_PATH):
        enrolled_emb = np.load(os.path.join(EMBS_PATH, emb_fname))
        emb_score = calc_score(emb, enrolled_emb)

        emb_fnames.append(emb_fname)
        emb_scores.append(emb_score)
    
    # - threshold compare and identify -----------------------------------------------------
    emb_fnames, emb_scores = np.array(emb_fnames), np.array(emb_scores)
    min_dist, min_idx = np.min(emb_scores), np.argmin(emb_scores)
    print(min_dist)
    ## if greater than THRESHOLD then it's UNKNOWN speaker 
    if min_dist > THRESHOLD:
        return None 
    ## else it's the speaker whose distance is the closest
    speaker_id = emb_fnames[min_idx].split('.')[0] 

    ### `metadata.csv` must exist  
    metadata_df = pd.read_csv(os.path.join(DATABASE_PATH, 'metadata.csv'))
    speaker_name = metadata_df[metadata_df['ID'] == int(speaker_id)]['name'].values[0]
    
    return (speaker_id, str(speaker_name))
