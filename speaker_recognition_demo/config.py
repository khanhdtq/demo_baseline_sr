import os 

SAMPLING_RATE = 16000
THRESHOLD = 0.5

VALIDATE_DIR = './speaker_recognition_demo/emb_voxceleb/train/X.SpeakerDiarization.VoxCeleb.train/validate_equal_error_rate/VoxCeleb.SpeakerVerification.VoxCeleb1_X.development'

DATABASE_PATH = './speaker_recognition_demo/database'
os.makedirs(DATABASE_PATH, exist_ok=True)
EMBS_PATH = os.path.join(DATABASE_PATH, 'embeddings')
os.makedirs(EMBS_PATH, exist_ok=True)
METADATA_PATH = os.path.join(DATABASE_PATH, 'metadata.csv')