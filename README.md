## Installation 
- Tạo conda enviroment với Python 3.7
- Install Torch 1.4.0
- Install pyannote-audio:
    ```
    git clone https://github.com/pyannote/pyannote-audio.git
    cd pyannote-audio
    git checkout develop
    pip install .
    ```
## Usage

    from speaker_recognition_demo import enroll, speaker_recognizer
    
- `speaker_recognizer(speech)`: `speech` là numpy array với dimension `(16000 * t,)`, trong đó t là duration của speech tính bằng giây (2 - 4 giây). Hàm này trả về:
    + `None`: Unknown speaker
    + `(id, speaker_name)`: trong đó `id` (kiểu `int`), và `speaker_name` (kiểu `str`) lần lượt là ID và họ tên của user đã đăng ký vào hệ thống trước đó.
- `enroll(wavs, speaker_name)`: `wavs` là 1 list chứa các numpy array, các numpy array này là các speech của cùng một user được thu để enroll, mỗi speech có dimension là (16000 * t,) (như trên); speaker_name là tên do user nhập. Hàm này sẽ thực hiện lưu user vào database và trả về dòng message thông báo ID và tên mà người đó đã được enroll với.

    